# GUIDE

## Command

Run services

```
docker-compose up -d [services]
```

Build service

```
docker-compose build --no-cache <service>
```

Run composer

```
docker run --rm --interactive --tty --volume $PWD:/app composer install
```
[Composer Hub](https://hub.docker.com/_/composer)